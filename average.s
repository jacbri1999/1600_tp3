.global matrix_row_aver_asm

matrix_row_aver_asm:
        push %ebp      		# Sauvegarde l'ancien pointeur ebp 
        mov %esp, %ebp 		# Fait pointer ebp au meme endroit que le pointeur esp 
        sub $12, %esp           # Alloue de l'espace pour les valeurs locals r, c et elem 
        push %ebx               # Mets le registre ebx sur la pile afin de l'utiliser
        push %esi               # Mets le registre esi sur la pile afin de l'utiliser
        push %edi               # Mets le registre edi sur la pile afin de l'utiliser
        movl $0, -4(%ebp)       # met la valeur de 0 dans r et c
        movl $0, -8(%ebp)
        movl 16(%ebp), %ebx     # Mets la valeur de matorder dans ebx
        movl 12(%ebp), %ecx     # Mets la valeur de outmatdata dans ecx
        movl 8(%ebp), %edi      # Mets la valeur de inmatdata dans edi
        jmp condition_premier_for

condition_premier_for:
        cmpl %ebx, -4(%ebp)     # Compare la valeur de r et matorder
        jl corps_premier_for    # Si la valeur de r est plus petite que matorder, jump corps_premier_for
        jmp fin_fonction

corps_premier_for:
        movl $0, -12(%ebp)      # Mets la valeur de 0 dans i
        jmp deuxieme_for

deuxieme_for:
        cmpl %ebx, -8(%ebp)      # Compare la valeur de matorder et c
        jl corps_deuxieme_for    # Si la valeur de c est plus petite que matorder, jump corps_deuxieme_for
        mov $0, %edx             # initialise les bits les plus significatifs a 0 
        movl -12(%ebp), %eax     # mets la valeur de elem dans eax
        idiv %ebx                # divise eax par la valeur contenue dans ebx (soit matorder)
        movl -4(%ebp), %esi      # Mets la valeur de r dans esi
        movl %eax, (%ecx, %esi, 4)  # Mets le resultat de la division (eax) dans (outmatdata + (esi*4))
        movl $0, -8(%ebp)
        jmp fin_for

		
corps_deuxieme_for:
        movl -4(%ebp), %eax      # Mets la valeur de r dans le registre eax
        imul %ebx, %eax          # Multiplie la valeur de ebx(matoder) et eax(r) et met le resultat dans eax
        addl -8(%ebp), %eax      # Additionne la valeur de c et eax
        movl (%edi, %eax, 4), %eax   # Mets la valeur de (inmatdata + (eax*4)) dans eax
        addl %eax, -12(%ebp)     # Additionne la valeur de eax et elem et mets le resultat dans elem
        incl -8(%ebp)            # Rajoute la valeur de 1 dans c (incrementation)
        jmp deuxieme_for
        

fin_for:
        incl -4(%ebp)                # Rajoute la valeur de 1 dans r (incrementation)
        jmp condition_premier_for


fin_fonction:
        pop %edi           # Pop les registres edi, esi et ebx pour desallouer la memoire
        pop %esi           
        pop %ebx          
        add $12, %esp      # Additionne esp de 12 pour etre a jour avec la pile
        leave              # Reinitialise ebp et esp 
        ret                # Return  
		
