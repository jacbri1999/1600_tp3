.globl matrix_multiply_asm

matrix_multiply_asm:
        push %ebp               # Sauvegarde l'ancien pointeur ebp 
        mov %esp, %ebp          # Fait pointer ebp au meme endroit que le pointeur esp  
        sub $16, %esp           # Alloue de l'espace pour les valeurs locales r, c, i et elem
        push %ebx               # Mets le registre ebx sur la pile afin de l'utiliser
        push %esi               # Mets le registre esi sur la pile afin de l'utiliser
        push %edi               # Mets le registre edi sur la pile afin de l'utiliser
        movl $0, -4(%ebp)       # Mets la valeur de 0 dans r,c, i et elem
        movl $0, -8(%ebp)
        movl $0, -12(%ebp) 
        movl $0, -16(%ebp) 
        movl 20(%ebp), %ebx    # Mets la valeur de matorder dans ebx
        movl 16(%ebp), %edx    # Mets la valeur de outmatdata dans edx
        movl 12(%ebp), %edi    # Mets la valeur de inmatdata2 dans edi
        movl 8(%ebp), %ecx     # Mets la valeur de inmatdata1 dans ecx
        jmp condition_premier_for

condition_premier_for:
        cmpl %ebx, -4(%ebp)   # compare la valeur de matorder et r
        jl deuxieme_for       # Si la valeur de r est plus petite que matorder, jump deuxieme_for
        jmp fin_fonction

deuxieme_for:
        cmpl %ebx, -8(%ebp)      # compare la valeur de matorder et c
        jl corps_deuxieme_for    # Si la valeur de c est plus petite que matoder, jump corps_deuxieme_for
        movl $0, -8(%ebp)
        jmp fin_for

		
corps_deuxieme_for:
        movl $0, -16(%ebp)
        jmp troisieme_for

troisieme_for:
        cmpl %ebx, -12(%ebp)     # Compare la valeur de matorder et i
        jl corps_troisieme_for   # SI la valeur de i est plus petite que matorder, jump corps_troisieme_for
        movl -4(%ebp), %eax      # Mets la valeur de r dans le registre eax
        imul %ebx, %eax          # Mutiplie ebx (matorder) par eax(r) et mets le resultat dans eax
        addl -8(%ebp), %eax      # Additionne la valeur de c et eax
        movl -16(%ebp), %esi     # Mets la valeur de elem dans le registre esi
        movl %esi, (%edx, %eax, 4)    # Mets la valeur de  esi (elem) dans (outmatdata + (eax*4)) 
        movl $0, -12(%ebp)
        incl -8(%ebp)            # Rajoute la valeur de 1 a c (incrementation)
        jmp deuxieme_for

corps_troisieme_for:
        movl -4(%ebp), %eax     # Mets la valeur ed r dans eax
        imul %ebx, %eax         # Multiplie ebx(matorder) par eax(r) et mets le resulat dans eax
        addl -12(%ebp), %eax    # Additionne la valeur de i et eax
        movl (%ecx, %eax, 4), %eax     # Mets la valeur de (inmatdata1 + (eax*4)) dans eax
        movl -12(%ebp), %esi    # Mets la valeur de i dans le registre esi
        imul %ebx, %esi         # Multiplie la valeur de ebx(matorder) par esi(i) et mets le resulat dans esi
        addl -8(%ebp), %esi     # Additionne la valeur de c et esi 
        movl (%edi, %esi, 4), %esi     # Mets la valeur de (inmatdata2 + (esi*4)) dans esi
        imul %eax, %esi         # Multiplie la valeur de eax par esi et mets le resultat dans esi
        addl %esi, -16(%ebp)    # Additionne esi par elem et mets le resultat dans elem
        incl -12(%ebp)          # Rajoute la valeur de 1 a i (incrementation)
        jmp troisieme_for
        

fin_for:
        incl -4(%ebp)           # Rajoute la valeur de 1 a r (incrementation)
        jmp condition_premier_for


fin_fonction:
        pop %edi                 # Pop les registres edi, esi et ebx pour desallouer la memoire
        pop %esi                 
        pop %ebx                 
        add $16, %esp            # Additionne esp de 16 pour etre a jour avec la pile
        leave                    # Reinitialise ebp et esp 
        ret                      # Return  

