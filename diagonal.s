.global matrix_diagonal_asm

matrix_diagonal_asm:
        push %ebp                # Sauvegarde l'ancien pointeur ebp 
        mov %esp, %ebp           # Fait pointer ebp au meme endroit que le pointeur esp  
        sub $8, %esp             # Alloue de l'espace pour les valeurs locales r et c 
        push %ebx                # Mets le registre ebx sur la pile afin de l'utiliser
        push %esi                # Mets le registre esi sur la pile afin de l'utiliser
        movl $0, -4(%ebp)        # Mets la valeur de 0 dans r et c
        movl $0, -8(%ebp)        
        movl 16(%ebp), %ebx      # Mets la valeur de matorder dans ebx
        movl 12(%ebp), %edx      # Mets la valeur de outmatdata dans edx
        movl 8(%ebp), %ecx       # Mets la valeur de inmatdata dans ecx
        jmp condition_premier_for   

condition_premier_for:
        cmpl %ebx, -4(%ebp)      # Compare la valeur de r et matorder
        jl deuxieme_for          # Si la valeur de r est plus petite que matorder, jump a deuxieme_for
        jmp fin_fonction

deuxieme_for:
        cmpl %ebx, -8(%ebp)      # Compare la valeur de c et matorder
        jl condition_if          # Si la valeur de r est plus petite que matorder, jump a deuxieme_for
        movl $0, -8(%ebp)
        jmp fin_for

condition_if:
        movl -4(%ebp), %eax        # Mets la valeur de r dans le registre eax
        cmpl %eax, -8(%ebp)        # Compare la valeur de r et c
        jne else                   # Si les valeurs ne sont pas egales, jump a else
        movl -4(%ebp), %eax        # Mets la valeur de r dans le registre eax
        imul %ebx, %eax            # Multiplie la valeur de ebx(matorder) et eax(r) et met le resultat dans le registre eax
        addl -8(%ebp), %eax        # Additionnne les valeurs de c et eax ensemble
        movl (%ecx, %eax, 4), %esi  # Mets (inmatdata + (eax*4)) dans le registre esi
        movl %esi, (%edx, %eax, 4)  # Mets esi dans (outmatdata + (eax*4))
        incl -8(%ebp)               # Rajoute la valeur de 1 dans c (incrementation)
        jmp deuxieme_for

else:
        movl -4(%ebp), %eax        # Mets la valeur de r dans le registre eax
        imul %ebx, %eax            # Multiplie la valeur de ebx(matorder) et eax(r) et met le resultat dans le registre eax
        addl -8(%ebp), %eax        # Additionnne les valeurs de c et eax ensemble
        movl $0, (%edx, %eax, 4)   # Mets 0 dans (outmatdata + (eax*4))
        incl -8(%ebp)              # Rajoute la valeur de 1 dans c (incrementation)
        jmp deuxieme_for
        

fin_for:
        incl -4(%ebp)                 # Rajoute la valeur de 1 dans r (incrementation)
        jmp condition_premier_for


fin_fonction:
        pop %esi                # Pop les registres esi et ebx pour desallouer la memoire
        pop %ebx               
        add $8, %esp            # Additionne esp de 8 pour etre a jour avec la pile
        leave                   # Reinitialise ebp et esp
        ret                     # Return 

