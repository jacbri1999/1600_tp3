.globl matrix_transpose_asm

matrix_transpose_asm:
        push %ebp                # Sauvegarde l'ancien pointeur ebp 
        mov %esp, %ebp           # Fait pointer ebp au meme endroit que le pointeur esp  
        sub $8, %esp             # Alloue de l'espace pour les valeurs locales r et c 
        push %ebx                # Mets le registre ebx sur la pile afin de l'utiliser
        push %esi                # Mets le registre esi sur la pile afin de l'utiliser
        movl $0, -4(%ebp)        # Mets la valeur de 0 dans r et c
        movl $0, -8(%ebp)        
        movl 16(%ebp), %ebx      # Mets la valeur de matorder dans ebx
        movl 12(%ebp), %edx      # Mets la valeur de outmatdata dans edx
        movl 8(%ebp), %ecx       # Mets la valeur de inmatdata dans ecx
        jmp condition_premier_for     

condition_premier_for:
        cmpl %ebx, -4(%ebp)      # Compare la valeur de r et matorder
        jl deuxieme_for          # Si la valeur de r est plus petite que matorder, jump a deuxieme_for
        jmp fin_fonction         

deuxieme_for:
        cmpl %ebx, -8(%ebp)      # Compare la valeur de c et matorder
        jl affectation           # Si la valeur de c est plus petite que matorder, jump a affectation
        movl $0, -8(%ebp)        
        jmp fin_for              


affectation:
        movl -8(%ebp), %eax      # Mets la valeur de c dans le registre eax
        imul %ebx, %eax          # Multiplie la valeur de ebx(matorder) et eax(c) et le met dans eax
        addl -4(%ebp), %eax      # Additionne la valeur de r et eax
        movl (%ecx, %eax, 4), %eax  # Mets (inmatdata + (eax*4)) dans le registre eax
        movl -4(%ebp), %esi      # Mets la valeur de r dans esi
        imul %ebx, %esi          # Multipie la valeur de ebx(matorder) et esi (r) et le met dans esi
        addl -8(%ebp), %esi      # Additionne la valeur de c et esi
        movl %eax, (%edx, %esi, 4)   # Met eax dans (outmatdata + (esi*4))
        incl -8(%ebp)            # Rajoute la valeur de 1 a c (incrementation)
        jmp deuxieme_for        

fin_for:
        incl -4(%ebp)            # Rajoute la valeur de 1 a r (incrementation)
        jmp condition_premier_for




fin_fonction:
        pop %esi       # Pop les registres esi et ebx pour desallouer la memoire
        pop %ebx       
        add $8, %esp   # Additionne esp de 8 pour être a jour avec la pile
        leave          # Reinitialise ebp et esp
        ret            # Return  
