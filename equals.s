.globl matrix_equals_asm

matrix_equals_asm:
        push %ebp                  # Sauvegarde l'ancien pointeur ebp
        mov %esp, %ebp             # Fait pointer ebp au meme endroit que le pointeur esp  
        sub $8, %esp               # alloue l'espace pour les variables locales r et c
        push %ebx                  # mets les registre ebx et esi sur la pile, afin de pouvoir les utiliser
        push %esi
        movl $0, -4(%ebp)          # initialise r et c à 0
        movl $0, -8(%ebp)   
        movl 16(%ebp), %ebx        # mets la valeur de matorder dans ebx
        movl 12(%ebp), %edx        # mets la valeur de inmatdata2 dans edx
        movl 8(%ebp), %ecx         # mets la valeur de inmatdata1 dans ecx
        jmp condition_premier_for  

condition_premier_for:
        cmpl %ebx, -4(%ebp)        # Compare la valeur de r et matorder
        jl deuxieme_for            # Si la valeur de r est plus petite que matorder, jump a deuxieme_for
        movl $1, %eax              # sinon, mets la valeur de retour a 1
        jmp fin_fonction           

deuxieme_for:
        cmpl %ebx, -8(%ebp)        # Compare la valeur de c et matorder
        jl condition_if            # Si la valeur de c est plus petite que matorder, jump a condition_if
        movl $0, -8(%ebp)       
        jmp fin_for                


condition_if:
        movl -4(%ebp), %eax          # Mets la valeur de r dans eax
        imul %ebx, %eax              # Multiplie la valeur de ebx(matorder) et eax(r) et le met dans eax
        addl -8(%ebp), %eax          # Additionne la valeur de c et eax et mets le resultat dans eax
        movl (%ecx, %eax, 4), %esi   # Mets (inmatdata1 + (eax*4)) dans le registre esi
        movl (%edx, %eax, 4), %eax   # Mets (inmatdata2 + (eax*4)) dans le registre eax
        cmpl %eax, %esi              # Compare la valeur des registres esi et eax
        jne return0                  # Jump si la valeur comparee n'est pas egal
        incl -8(%ebp)                # Rajoute la valeur de 1 dans c (incrementation)
        jmp deuxieme_for             

return0:
        movl $0, %eax                # Mets la valeur de retour a 0
        jmp fin_fonction          

fin_for:
        incl -4(%ebp)                # Rajoute la valeur de 1 dans r (incrementation)
        jmp condition_premier_for  




fin_fonction:
        pop %esi          # Pop les registres esi et ebx pour desallouer la memoire
        pop %ebx         
        add $8, %esp      # Additionne esp de 8 pour etre a jour avec la pile
        leave             # Reinitialise ebp et esp
        ret               # Return  
